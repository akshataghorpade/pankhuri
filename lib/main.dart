import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightGreen[100],
      body:
      ListView(
        children: <Widget>[
          SizedBox(height: 15.0),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: const EdgeInsets.only(
                  bottom:0,
                  right: 10,
                  left: 0,top: 5
              ),
              child: Icon(
                Icons.clear,
                color: Colors.lightGreen[800],
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: const EdgeInsets.only(
                  bottom:10,
                  right: 0,
                  left: 0,top: 50
              ),
              child:   Text(
                "Order Complete",
                style: TextStyle(color: Colors.lightGreen[800], fontSize: 25, fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
            ),
          ),

          Container(
              padding: EdgeInsets.only(right: 15.0,left: 15.0, top:10),
              width: MediaQuery.of(context).size.width - 30.0,
              height: MediaQuery.of(context).size.height - 600.0,
              child:
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),

                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      Container(

                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                            ),
                            child: Image.asset(
                              "assets/tag.jpg",
                              height: 60,
                              width: 60,
                            ),
                          ),
                        ),
                        height: MediaQuery.of(context).size.width - 220,
                        width: MediaQuery.of(context).size.width - 220,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/download.jpg'),
                              fit: BoxFit.cover
                          ),
                        ),
                      ),

                      Expanded(
                        child:
                        Column(
                          mainAxisSize:MainAxisSize.min,
                          crossAxisAlignment:CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(left:10),
                              child: Text(
                                "Baking wedding cakes.",
                                style: TextStyle(color: Colors.black, fontSize: 16,fontWeight: FontWeight.bold),
                                textAlign: TextAlign.left,
                              ),
                            ),

                            Padding(
                              padding: EdgeInsets.only(left:10,top:5,bottom: 5),
                              child: Text("DIY recipies for your big day. ",
                                  style: TextStyle(color: Colors.black, fontSize: 16,fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.left),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left:10.0),
                              child: Text(
                                "Take control",
                                style: TextStyle(color: Colors.black, fontSize: 16,fontWeight: FontWeight.bold),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left:10,top:5,bottom: 5,),
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    "27-30 July",
                                    style: TextStyle(color: Colors.pink, fontSize: 16,fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.left,
                                  ),
                                  Container(height: 20, child: VerticalDivider(color: Colors.pink)),
                                  Text(
                                    "3.30-4.30 PM",
                                    style: TextStyle(color: Colors.pink, fontSize: 14),
                                    textAlign: TextAlign.left,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                    ],
                  ),
                ),
              )
          ),
          SizedBox(height: 15.0),
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisSize:MainAxisSize.min,
              crossAxisAlignment:CrossAxisAlignment.center,
              children: <Widget>[

                Text(
                  "Your booking is confirmed.",
                  style: TextStyle(color: Colors.lightGreen[800], fontSize: 20,fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),

                Padding(
                  padding: EdgeInsets.all(10),
                  child:    Text("You can find all the details",
                    style: TextStyle(color: Colors.lightGreen[800], fontSize: 20,fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  "of your booking under 'Orders & Booking'",
                  style: TextStyle(color: Colors.lightGreen[800], fontSize: 20,fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                  child:  Text(
                    "in the menu",
                    style: TextStyle(color: Colors.lightGreen[800], fontSize: 20,fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        bottom:10,
                        right: 0,
                        left: 0,top: 250
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround ,
                      children: <Widget>[
                        // Expanded(
                        new SizedBox(
                          width: 150.0,
                          child: RaisedButton(
                            child: Text('View Booking'),
                            color: Colors.lightGreen[800],
                            textColor: Colors.white,

                            padding: EdgeInsets.all(15),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(30.0))),
                            onPressed: () => null,
                          ),
                        ),
                        // Expanded(
                        new SizedBox(
                          width: 150.0,
                          child: RaisedButton(
                            child: Text('Explore More'),
                            textColor: Colors.white,
                            padding: EdgeInsets.all(15),
                            color: Colors.lightGreen[800],
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(30.0))),
                            onPressed: () => null,
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
